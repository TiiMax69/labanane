<?php
	session_start();
	$mois_json = file_get_contents('./mois.json');
	$mois_json = json_decode($mois_json,true);
	if(!isset($_SESSION['id_abo_labanane']))
		header('Location: log_in');
	include './SQL/bdd_connexion.php';
	$query    = $bdd -> query('SELECT num_abonne,ville_nom_reel,ville_id 
							   FROM abonnes a 
							   		LEFT JOIN villes_france vf 
							   			ON a.id_ville=vf.ville_id 
							   	WHERE id_abonne='.$_SESSION['id_abonne']);
	$data     = $query -> fetch();
	$number   = '0'.substr($data['num_abonne'],3);
	$ville    = $data['ville_nom_reel'];
	$id_ville = $data['ville_id'];
?>
<html>
	<head>
		<?php include "meta.html"; ?>
		<title>
			La banane
		</title>
  		<script src="jquery.js"></script>
 		 <script src="./script/main_script.js"></script>
		 <link rel="stylesheet" href="jquery-ui.css"/>
		<link rel="stylesheet" href="./connexion.css" />
		 <script src="jquery-ui.js"></script>
	</head>
	<body>
	<header class="col-sm-12">
		<img id="title-font" alt="logo text" src="./images/banane.png">
	</header>
	<div id="user-info" class=" col-sm-3">
		<h2>Info perso :</h2>
		<?php
		$query = $bdd -> query('SELECT num_abonne,nom,prenom,email FROM abonnes WHERE id_abonne='.$_SESSION['id_abonne'].' LIMIT 1');
		$data  = $query -> fetch();
		echo '<cellphone>'.$data['num_abonne'].'</cellphone><br/>
		<h3>Nom </h3><div class="input"><input class="user-info" id="nom" value="'.$data['nom'].'"><div class="focus"></div></div>
		<h3>Prénom </h3><div class="input"><input class="user-info" id="prenom" value="'.$data['prenom'].'"><div class="focus"></div></div>
		<h3>Email </h3><div class="input"><input class="user-info" id="email" value="'.$data['email'].'"><div class="focus"></div></div>
		<h3>Ville </h3><div class="input"><input class="user-info" id="ville" id_ville="'.$id_ville.'" value="'.$ville.'"><div class="focus"></div></div>';
		?>
	</div>
	<article class="col-sm-9">
	<nav class="col-sm-12" id="select_month">
		<button class="back month"><span class="glyphicon glyphicon-chevron-left"></span></button><?php 
		$m_now = date('n');	
		for($i=0;$i<4;$i++)
			$m_now = get_month_button($m_now);
		?><button class="next month"><span class="glyphicon glyphicon-chevron-right"></span></button>
	</nav>
	</article>
	<footer>
		
	</footer>
	</body>
</html>
<?php
 function get_month_button($mois)
 {
 	$selected = $mois == date('n') ? 'selected' : '';
 	global $mois_json;
 	echo '<button class="month '.$selected.'">'.$mois_json[$mois]['name'].'</button>';
 	return ($mois == 12) ? 1 : ++$mois;
 }
?>