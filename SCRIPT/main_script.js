function disconnect()
{
	$.ajax({
		url:'./disconnect.php'
	}).done(function(){location.href = './index.html';});
}
function save_informations()
{
	var id_ville = document.getElementById('ville').getAttribute('id_ville');
	$.ajax({
		url:'./update/update_informations.php',
		type:'POST',
		data:'id_ville='+id_ville
	}).done(function(){
		location.href = './main.php';
	});
}
$(function() {
	$('#ville').autocomplete({
			source:'./found_city.php',
			minLength  : 1,
			maxResults : 10,
			select: function(event, ui) 
			{
				document.getElementById('ville').setAttribute('id_ville',ui.item.id_ville);
			}
		});
});