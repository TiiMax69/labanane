<?php
	if(isset($_SESSION['id_abo_labanane']))
		header('Location: ./');
?>
<html>
	<head>
		<?php include "meta.html"; ?>
		<link rel="stylesheet" href="./home.css" />
		<title>
			La banane - connexion
		</title>
	</head>
	<body>
	<div>
		<div id="social_network">
			<a class="btn btn-sm btn-social-icon btn-twitter">
			    <span class="fa fa-twitter"></span>
			</a>
			<a class="btn btn-sm btn-social-icon btn-instagram">
			    <span class="fa fa-instagram"></span>
			</a>
			<a class="btn btn-sm btn-social-icon btn-facebook" target="_blank" href="https://www.facebook.com/LaBanane38/?hc_ref=SEARCH">
			    <span class="fa fa-facebook"></span>
			</a>
		</div>

		<div id="title" class="col-sm-12">
			<img id="title-font" alt="logo text" src="./images/banane.png">
		</div>
	</div>
	<div class="col-sm-4">
		<div class="row">
			<h2>La banane c'est quoi?</h2>
			<span>C'est un collectif Isérois à but non lucratif, qui vous propose un service gratuit d'infos-sorties par SMS, pour vous simplifier la vie !</span>
		</div>
		<div class="row">
			<h2>Pour qui?</h2>
			<span>Tout le monde peut en profiter ! Amateurs de spectacles, de concerts ou sorties culturelles, la Banane vous partage ses sorties coup de coeur près de chez vous !</span>
		</div>
	</div>
	<div id="content" class="col-sm-4">
		<div id="form-connexion">
			<form action="try_connexion" method="post">
				<table>
					<tr>
						<td><input type="text" name="id" placeholder="N° tel" class="form-control" id="user" pattern="[0-9]{10,14}"></td>
						<td><input type="password" name="password" placeholder="Mot de passe" class="form-control" id="password"></td>
					</tr>
					<tr>
						<td colspan="2" id="lost">
							<a>Mot de passe oublié?</a>
						</td>
					</tr>
					<tr>
						<td colspan=2 class="td-submit">
							<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-chevron-right"></span></button>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="row">
			<h2>Comment?</h2>
			<span>Pour recevoir l'info SMS de La banane, il suffit de s'inscrire ... par SMS</span>
		</div>
		<div class="row">
			<h2>Inscrivez vous!*</h2>
			<span>Envoyez "banane" par SMS au 07 69 33 97 62.<br/><strong>*Service totalement gratuit</strong></span>
		</div>
	</div>
	<footer>
		Copyright ©
	</footer>
	</body>
</html>